// to adding custom HTML code injector pluggin // can be used via pluggins
var Hcj_builder_HTML_injector = (builder) => {
    let pluginId = 'inject-html-modal'
    if (builder.Plugins.find( item => item == pluginId )){
        console.log(`Pluggin: "${pluginId}" is already installed!`)
        return
    }
    // creating a modal for injector
    let editor = document.createElement('textarea')
    let closeBtn = document.createElement('button')
    let modalObj = builder.addModal({
        title: [
            { tagName: "span", components: "HTML Injector" },
            {
                tagName: 'div', components: [
                    {
                        tagName: "button",
                        components: "Import",
                        attributes: { title: "Inject this HTML Code" },
                        events: [
                            {
                                type: 'click', action(e) {
                                    builder.pasteHTML(editor.value)
                                }
                            }
                        ]
                    },
                    {
                        tagName: 'button',
                        components: "Export",
                        attributes: { title: "Load the current HTML content onto the editor" },
                        events: [
                            {
                                type: 'click', action(e) {
                                    editor.value = builder.getHtmlString()
                                }
                            }
                        ]
                    }
                ]
            },
            { tagName: 'div', components: [{ DOM: closeBtn, components: "Close", attributes: { title: "Close Injector Modal" } }] }
        ],
        body: [
            {
                DOM: editor,
                events: [{
                    type: "keydown",
                    action(e) {
                        if (e.key === 'Tab') {
                            e.preventDefault(); // Prevent the default behavior (focus switch)

                            // Insert a tab character ('\t') at the current caret position
                            var start = this.selectionStart;
                            var end = this.selectionEnd;
                            var value = this.value;
                            this.value = value.substring(0, start) + '\t' + value.substring(end);
                            this.selectionStart = this.selectionEnd = start + 1;
                        }
                    }
                }]
            }
        ],
    }, 'inject-html-modal')
    modalObj.closeBtn = closeBtn
    modalObj.elem = modalObj.elem
    modalObj.opened = false;
    modalObj.editor = editor
    closeBtn.addEventListener('mousedown', modalObj.hide)

    builder.Navbar.add({
        tagName: "button",
        components: "💉",
        attributes: {
            class: "btn",
            title: "Toggle Html Injector",
        },
        events: [{ type: "click", action: modalObj.show }]
    })

    builder.Plugins.push(pluginId)

}