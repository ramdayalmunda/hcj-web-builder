let builder = Hcj_builder()
builder.mount('container-for-builder')
Hcj_builder_HTML_injector(builder); // this is to include the HTML injector library
Hcj_builder_block_manager(builder); // this is to install block manager plugin to hcj builder
let content = [
    {
        tagName: "head",
        components: [
            { tagName: "title", components: "Created web-page" },
            {
                tagName: "link",
                components: "",
                attributes: {
                    "href": "https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css",
                    "rel": "stylesheet"
                }
            },
            {
                tagName: "script",
                components: "",
                attributes: { "src": "https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" }
            },
        ]
    },
    {
        tagName: "body",
        attributes: {
            class: "container"
        },
        components: [
            {
                tagName: "div",
                components: [
                    {
                        tagName: "div",
                        components: [
                            { tagName: "span", components: "Hey! You seem to be in the" },
                            { tagName: "b", components: " RIGHT" },
                            { tagName: "span", components: " Place." },
                        ],
                    },
                    {
                        tagName: 'h1',
                        attributes: { class: "h1" },
                        components: [
                            "This is my webpage",
                            { tagName: "i", components: "italics" },
                        ]
                    },
                    { tagName: "p", components: "what is google analitics" },
                    {
                        tagName: "p",
                        components: [
                            "Google Analytics is a web analytics service offered by Google that allows website owners and marketers to track and analyze various aspects of website traffic and user behavior. It provides valuable insights into how users interact with a website, helping businesses and website owners make data-driven decisions to improve their online presence and user experience.",
                            "<br>",
                            {
                                tagName: "ol",
                                components: [
                                    {
                                        tagName: "li",
                                        components: [
                                            { tagName: "b", components: "Traffic Analysis:" },
                                            "Google Analytics helps you understand where your website traffic is coming from, whether it's from search engines, social media, direct visits, or other sources."
                                        ]
                                    },
                                    {
                                        tagName: "li",
                                        components: [
                                            { tagName: "b", components: "User Behavior:" },
                                            "It tracks user actions on your website, such as pageviews, clicks, and interactions with specific elements like forms or videos. This information helps you analyze what content or features are most popular and engaging for your audience."
                                        ]
                                    },
                                    {
                                        tagName: "li",
                                        components: [
                                            { tagName: "b", components: "Audience Insights::" },
                                            "You can gain insights into your website's audience demographics, interests, and geographic locations. This information is essential for tailoring your content and marketing efforts to your target audience"
                                        ]
                                    },
                                    {
                                        tagName: "li",
                                        components: [
                                            { tagName: "b", components: "Conversion Tracking:" },
                                            "Google Analytics allows you to set up and track specific goals and conversions, such as completing a purchase, signing up for a newsletter, or downloading a resource. You can see which marketing campaigns or pages are driving these conversions."
                                        ]
                                    },
                                    {
                                        tagName: "li",
                                        components: [
                                            { tagName: "b", components: "E-commerce Tracking:" },
                                            "For online businesses, Google Analytics provides robust e-commerce tracking features to monitor sales, revenue, and product performance."
                                        ]
                                    },
                                    {
                                        tagName: "li",
                                        components: [
                                            { tagName: "b", components: "Real-time Data:" },
                                            "You can view real-time data to see how users are currently interacting with your website. This is useful for monitoring live campaigns and events."
                                        ]
                                    },
                                ]
                            }
                        ]
                    },
                    {
                        tagName: "div",
                        components: [
                            {
                                tagName: 'h3', components: "These are the scores of the students on various subjects"
                            },
                            {
                                tagName: "table",
                                components: [
                                    {
                                        tagName: "thead",
                                        components: [
                                            {
                                                tagName: "tr", components: [
                                                    { tagName: "th", components: "#" },
                                                    { tagName: "th", components: "Name" },
                                                    { tagName: "th", components: "Hindi" },
                                                    { tagName: "th", components: "Maths" },
                                                    { tagName: "th", components: "Science" },
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        tagName: "tbody",
                                        components: [
                                            {
                                                tagName: "tr", components: [
                                                    { tagName: "td", components: "1" },
                                                    { tagName: "td", components: "Anil" },
                                                    { tagName: "td", components: "54" },
                                                    { tagName: "td", components: "53" },
                                                    { tagName: "td", components: "66" },
                                                ]
                                            },
                                            {
                                                tagName: "tr", components: [
                                                    { tagName: "td", components: "2" },
                                                    { tagName: "td", components: "Ayush" },
                                                    { tagName: "td", components: "54" },
                                                    { tagName: "td", components: "24" },
                                                    { tagName: "td", components: "46" },
                                                ]
                                            },
                                            {
                                                tagName: "tr", components: [
                                                    { tagName: "td", components: "3" },
                                                    { tagName: "td", components: "Prince" },
                                                    { tagName: "td", components: "65" },
                                                    { tagName: "td", components: "56" },
                                                    { tagName: "td", components: "65" },
                                                ]
                                            },
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ],
                attributes: {
                    class: "container-fluid mt-4 mb-4"
                }
            }
        ]
    }
]

builder.loadData(content)

// this is how you add a custom button on navbar
let navbar = builder.Navbar
navbar.add({
    tagName: "button",
    components: "Save",
    attributes: {
        class: "btn",
        title: "Save Data",
    },
    events: [
        {
            type: "click",
            action: () => {
                let pageData = builder.getData()
                console.log(pageData)
            }
        }
    ]
})
window['builder'] = builder; // exposing on console