var Hcj_builder = function () {
    var container = null;
    var mainDiv = null;
    var content = null;
    var iframe = null;
    var body = null;
    let defaultIframe = /*html*/`
<head>
    <title>Your webpage</title>
</head>
<body>
    <h2>Custom content here</h2>
</body>
    `
    var SelectedDom = null;
    var PhantomElem = { cover: null, nameTag: null, optionBar: null };

    var navbar = {
        elem: null,
        defaultComponents: [
            {
                tagName: "button",
                components: "🗄️",
                attributes: {
                    class: "btn",
                    title: "Toggle sidebar",
                },
                events: [
                    {
                        type: "click", action: () => {
                            sidebar.opened = !sidebar.opened
                            if (sidebar.opened) sidebar.elem.classList.remove('closed')
                            else sidebar.elem.classList.add('closed')
                        }
                    }
                ]
            },
            {
                tagName: "button",
                components: "⛶",
                attributes: {
                    class: "btn",
                    title: "Full Screen",
                },
                events: [
                    {
                        type: "click", action: () => {
                            iframe.requestFullscreen()
                        }
                    }
                ]
            }
        ],
        add(component) {
            let domElem = createDom(component)
            navbar.elem.append(domElem)
            return domElem
        }
    };
    var sidebar = {
        elem: null,
        opened: true,
        components: [],
        counter: 0,
        add(obj) {
            let sidebarObj = {
                elem: createDom(obj),
                id: `hcj-sidebar-${new Date().getTime()}`,
            }
            sidebar.elem.append(sidebarObj.elem)
            sidebarObj.show = () => this.show(sidebarObj.id)
            this.components.push(sidebarObj)
            return sidebarObj
        },
        hideAll() {
            for (let i = 0; i < sidebar.components.length; i++) {
                sidebar.components[i].elem.style.display = 'none'
            }
        },
        show(id) {
            console.log('sidebar', this.components, id)
            this.hideAll()
            let getElem = this.components.find(item => item.id == id)
            if (getElem) {
                let display = getElem.elem.getAttribute('display')
                if (display == 'yes') {
                    getElem.elem.setAttribute('display', 'no')
                    getElem.elem.style.display = 'none'
                } else {
                    getElem.elem.style.display = 'block'
                    getElem.elem.setAttribute('display', 'yes')
                }
            }
        },
    }

    var Modals = []
    var Plugins = []

    function initialize() {
        // create iframe
        mainDiv = document.createElement('div')
        mainDiv.setAttribute('class', 'hcj-builder main')
        container.append(mainDiv)

        // adding navbar
        !(() => {

            navbar.elem = document.createElement('div')
            navbar.elem.setAttribute('class', 'hcj-builder navbar')
            for (const obj of navbar.defaultComponents) navbar.elem.append(createDom(obj));
            mainDiv.append(navbar.elem)


        })()

        body = document.createElement('div')
        body.setAttribute('class', 'hcj-builder body')

        // to create the sidebar
        !(async () => {
            sidebar.elem = document.createElement('div')
            sidebar.elem.setAttribute('class', 'hcj-builder sidebar')

            body.append(sidebar.elem)
        })()

        //to create the main window
        !(() => {
            content = document.createElement('div')
            content.setAttribute('class', 'hcj-builder content')
            iframe = document.createElement('iframe')
            iframe.setAttribute('class', 'hcj-builder')
            content.append(iframe)
            body.append(content)
        })()

        // create the phantom elem// this is the element that will be shown on the UI when a dom is selected
        !(() => {
            PhantomElem.cover = document.createElement('div')
            PhantomElem.cover.style.position = "none"
            PhantomElem.cover.style.position = "absolute"
            PhantomElem.cover.style.border = "solid 3px"
            PhantomElem.cover.style.top = "20px"
            PhantomElem.cover.style.left = "20px"
            PhantomElem.cover.style.height = "20px"
            PhantomElem.cover.style.width = "20px"
            PhantomElem.cover.style.opacity = "0.5"
            PhantomElem.cover.style.pointerEvents = 'none'
            PhantomElem.cover.classList.add('color-quirk')
            content.append(PhantomElem.cover)
            console.log('content', content)
        })()


        mainDiv.append(body)
    }

    function addModal(modalData, modalId) {

        let modalMain = document.createElement('div')
        modalMain.setAttribute('class', 'modal')

        let dialog = document.createElement('div')
        dialog.setAttribute('class', 'modal-dialog')
        modalMain.append(dialog)

        let modalTitle = document.createElement('div')
        modalTitle.setAttribute('class', 'modal-title')
        for (let i = 0; i < modalData.title.length; i++) modalTitle.append(createDom(modalData.title[i]))
        dialog.append(modalTitle)

        let modalContent = document.createElement('div')
        modalContent.setAttribute('class', 'modal-content')
        dialog.append(modalContent)
        for (let i = 0; i < modalData.body.length; i++) modalContent.append(createDom(modalData.body[i]))

        body.append(modalMain)
        let modalObj = {
            elem: modalMain,
            id: modalId ? modalId : `modal-${new Date().getTime()}`,
            title: modalTitle,
            content: modalContent,
            isActive() { return modalMain.classList.contains('active') },
        }
        modalObj.toggle = () => {
            modalMain.classList.toggle('active')
        }
        modalObj.show = () => {
            modalMain.classList.add('active')
        }
        modalObj.hide = () => {
            modalMain.classList.remove('active')
        }
        Modals.push(modalObj)
        return modalObj
    }

    function loadHtmlString(stringHtml) {
        let doc = iframe.contentWindow.document || iframe.contentDocument
        doc.open()
        doc.write(stringHtml)
        doc.close()
    }

    function handleAccordions(e) {
        // this is to handle accordions on the sidebar
        let targetId = e.target.getAttribute('hcj-target')
        if (e.target.getAttribute('hcj-toggle') == 'accordion' && targetId) {
            let target = document.getElementById(targetId)
            if (target) {

                console.log('found accordion')
                let isActive = target.classList.contains('active')
                for (const item of target.children) {
                    if (item.classList.contains('content')) {
                        item.style.height = isActive ? '0px' : `${item.scrollHeight}px`
                    }
                }
                target.classList.toggle('active')
            }

        }
    }

    function handlerMousemove(e) {
        phantomHandler(e)
    }

    function phantomHandler(e){
        var xPos = e.clientX;
        var yPos = e.clientY;

        SelectedDom = e.target
        let rect = SelectedDom.getBoundingClientRect()
        PhantomElem.cover.style.width = `${rect.width}px`
        PhantomElem.cover.style.height = `${rect.height}px`
        PhantomElem.cover.style.left = `${rect.left}px`
        PhantomElem.cover.style.top = `${rect.top}px`

    }

    function addListeners() {
        mainDiv.addEventListener('click', handleAccordions)

        let doc = iframe.contentWindow.document || iframe.contentDocument
        iframe.addEventListener('load', function () {
            doc.addEventListener('mousemove', handlerMousemove)
        })
    }
    function removeListeners() {
        mainDiv.removeEventListener('click', handleAccordions)
        let doc = iframe.contentWindow.document || iframe.contentDocument
        doc.removeEventListener('mousemove', handlerMousemove)
    }

    function createDom(obj) {
        let elem = obj?.DOM
        if (!elem) elem = document.createElement(obj.tagName)

        // adding items inside this tag
        if (typeof obj.components == 'string') elem.append(obj.components)
        else {
            for (let i = 0; i < obj?.components?.length; i++) {
                if (typeof obj.components[i] == 'string') elem.append(obj.components[i])
                else elem.append(createDom(obj.components[i]))
            }
        }

        // setting up attributes
        if (obj?.attributes)
            for (const key in obj.attributes)
                elem.setAttribute(key, obj.attributes[key])

        //adding event listeners
        if (obj?.events)
            for (const event of obj.events)
                if (event.type && event.action)
                    elem.addEventListener(event.type, event.action)

        return elem
    }

    function getHtmlString() {
        let doc = iframe.contentWindow.document || iframe.contentDocument
        return `${doc.head.outerHTML}\n${doc.body.outerHTML}`
    }

    function getDOMJSONObj(domElem) {
        let jsonObj = { tagName: domElem.tagName }

        // getting attributes
        jsonObj.attributes = {}
        for (let i = 0; i < domElem.attributes.length; i++) {
            let value = domElem.attributes[i].value
            if (value) jsonObj.attributes[domElem.attributes[i].name] = value
        }

        jsonObj.components = []
        let childNodes = domElem.childNodes
        if (childNodes?.length)
            for (let i = 0; i < childNodes.length; i++) {
                if (typeof childNodes[i].data == 'string') jsonObj.components.push(childNodes[i].data)
                else jsonObj.components.push(getDOMJSONObj(childNodes[i]))
            }
        if (jsonObj.components.length == 1 && typeof jsonObj.components[0] == 'string') jsonObj.components = jsonObj.components[0]
        return jsonObj
    }

    var returnObj = {
        Navbar: navbar,
        Sidebar: sidebar,
        Modals,
        Plugins,
        mount(id, options) {
            container = document.getElementById(id)
            initialize()
            addListeners()
        },
        unMount() {
            removeListeners()
        },
        pasteHTML: loadHtmlString,
        addModal,
        loadData(obj) {

            let htmlString = ''
            for (let i = 0; i < obj.length; i++) {
                let domElem = createDom(obj[i])
                htmlString += domElem.outerHTML
            }
            loadHtmlString(htmlString)

        },
        getData() {

            let doc = iframe.contentWindow.document || iframe.contentDocument
            doc = doc.documentElement
            let JSONObj = getDOMJSONObj(doc)
            return JSONObj.components
        },
        getHtmlString,
    }

    return returnObj
}