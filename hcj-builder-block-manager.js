var Hcj_builder_block_manager = (builder)=>{
    let pluginId = "hcj-builder-block-manager"
    if (builder.Plugins.find( item => item==pluginId )){
        console.log(`Pluggin: "${pluginId}" is already installed!`)
        return
    }

    // // adding custom sidebar component
    let sidebarObj = builder.Sidebar.add({
        tagName: "div",
        components: [
            {
                tagName: "div",
                attributes: { class: "hcj-block-manager container" },
                components: [
                    { tagName: "span", components: "<div/>", attributes: { class: "hcj-block-manager draggable" } },
                    { tagName: "span", components: "<span/>", attributes: { class: "hcj-block-manager draggable" } },
                    { tagName: "span", components: "<a/>", attributes: { class: "hcj-block-manager draggable" } },
                    { tagName: "span", components: "<table/>", attributes: { class: "hcj-block-manager draggable" } },
                    { tagName: "span", components: "<thead/>", attributes: { class: "hcj-block-manager draggable" } },
                    { tagName: "span", components: "<tbody/>", attributes: { class: "hcj-block-manager draggable" } },
                    { tagName: "span", components: "<tr/>", attributes: { class: "hcj-block-manager draggable" } },
                    { tagName: "span", components: "<th/>", attributes: { class: "hcj-block-manager draggable" } },
                    { tagName: "span", components: "<td/>", attributes: { class: "hcj-block-manager draggable" } },
                ]
            }
        ]
    })

    // // adding custom navbar button to toggle
    let navBtn = builder.Navbar.add({
        tagName: "button",
        components: "🧩",
        attributes: {
            title: "Block Manager"
        },
        events: [ {
            type: "click",
            action(e){
                sidebarObj.show()
            }
        }]
    })
}