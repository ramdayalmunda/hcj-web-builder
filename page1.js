document.addEventListener('click', (e)=>{
    let targetId = e.target.getAttribute('hcj-target')
    if (e.target.getAttribute('hcj-toggle')=='accordion' && targetId){
        let target = document.getElementById(targetId)
        if (target){
            let isActive = target.classList.contains('active')
            for ( const item of target.children ){
                if (item.classList.contains('content')){
                    console.log('contnt', item.scrollHeight, isActive)
                    item.style.height = isActive?'0px':`${item.scrollHeight}px`
                }
            }
            target.classList.toggle('active')
        }
        
    }
})